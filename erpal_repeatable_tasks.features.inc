<?php
/**
 * @file
 * erpal_repeatable_tasks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function erpal_repeatable_tasks_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_date_item_type().
 */
function erpal_repeatable_tasks_default_date_item_type() {
  $items = array();
  $items['repeat_task_date'] = entity_import('date_item_type', '{
    "name" : "repeat_task_date",
    "label" : "repeat_task_date",
    "locked" : "0",
    "weight" : "0",
    "data" : null
  }');
  return $items;
}
