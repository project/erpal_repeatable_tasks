<?php
/**
 * @file
 * erpal_repeatable_tasks.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function erpal_repeatable_tasks_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'date_item-repeat_task_date-field_task_ref'
  $field_instances['date_item-repeat_task_date-field_task_ref'] = array(
    'bundle' => 'repeat_task_date',
    'comment_enabled' => 0,
    'default_value' => NULL,
    'default_value_function' => '',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'date_item',
    'field_name' => 'field_task_ref',
    'label' => 'Repeating task',
    'required' => 0,
    'settings' => array(
      'behaviors' => array(
        'prepopulate' => array(
          'status' => 0,
        ),
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'field_extrawidgets',
      'settings' => array(
        'display_empty' => 0,
        'formatter' => 'entityreference_label',
        'formatter_settings' => array(
          'link' => 0,
        ),
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'field_extrawidgets_read_only',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-erpal_task-field_increase_repeated_deadline'
  $field_instances['node-erpal_task-field_increase_repeated_deadline'] = array(
    'bundle' => 'erpal_task',
    'comment_enabled' => 0,
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'This is the amount of days added to the repeating date to set the tasks deadline',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 23,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'in_activity' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'minimal' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'view_row_details' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_increase_repeated_deadline',
    'label' => 'Increase repeated deadline',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'maxlength_js' => 0,
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'size' => 10,
      ),
      'type' => 'text_textfield',
      'weight' => 22,
    ),
  );

  // Exported field_instance: 'node-erpal_task-field_repeat_task'
  $field_instances['node-erpal_task-field_repeat_task'] = array(
    'bundle' => 'erpal_task',
    'comment_enabled' => 0,
    'date_item' => array(
      'create_date_item' => 1,
      'date_item_type' => 'repeat_task_date',
      'handle_range' => 'range',
    ),
    'deleted' => 0,
    'description' => 'This is a repeatable date option to repeat tasks. The first repeated task will be at this date.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 22,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'in_activity' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'minimal' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'view_row_details' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_repeat_task',
    'label' => 'Repeat task',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'repeat_collapsed' => 1,
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 20,
    ),
  );

  // Exported field_instance: 'node-erpal_task-field_subtask_repeat_options'
  $field_instances['node-erpal_task-field_subtask_repeat_options'] = array(
    'bundle' => 'erpal_task',
    'comment_enabled' => 0,
    'default_value' => array(
      0 => array(
        'value' => 'repeat',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 21,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'in_activity' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'minimal' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'view_row_details' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_subtask_repeat_options',
    'label' => 'Subtask repeat  options',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'options_select',
      'weight' => 21,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Increase repeated deadline');
  t('Repeat task');
  t('Repeating task');
  t('Subtask repeat  options');
  t('This is a repeatable date option to repeat tasks. The first repeated task will be at this date.');
  t('This is the amount of days added to the repeating date to set the tasks deadline');

  return $field_instances;
}
