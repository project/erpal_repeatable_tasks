<?php
/**
 * @file
 * erpal_repeatable_tasks.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function erpal_repeatable_tasks_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_task_repeat_options|node|erpal_task|form';
  $field_group->group_name = 'group_task_repeat_options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'erpal_task';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Repeat options',
    'weight' => '21',
    'children' => array(
      0 => 'field_repeat_task',
      1 => 'field_subtask_repeat_options',
      2 => 'field_increase_repeated_deadline',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Repeat options',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_task_repeat_options|node|erpal_task|form'] = $field_group;

  return $export;
}
